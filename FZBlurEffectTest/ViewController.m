//
//  ViewController.m
//  FZBlurEffectTest
//
//  Created by Fan Zhang on 6/9/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
            

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    _visualEffectView.frame = self.view.frame;
    [self.view insertSubview:_visualEffectView belowSubview:self.blurMeButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)blurMeButtonPressed:(id)sender {
    
    _visualEffectView.hidden = !_visualEffectView.hidden;
    
}

@end
