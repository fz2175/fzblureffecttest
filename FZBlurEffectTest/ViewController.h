//
//  ViewController.h
//  FZBlurEffectTest
//
//  Created by Fan Zhang on 6/9/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    
    UIVisualEffectView *_visualEffectView;
    
}

@property (weak, nonatomic) IBOutlet UIButton *blurMeButton;


@end

